package com.browserstack;

import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.offset.PointOption;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocalTest extends BrowserStackTestNGTest {

	@Test
	public void method() {
		log.info("Home page is displayed");
		MobileElement el0 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/Button_browse_gifts");
		el0.click();
		log.info("Browse Gifts button is selected");
		MobileElement el1 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/zipcode");
		el1.sendKeys("10011");
		MobileElement el2 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/dropdownButton");
		el2.click();
		MobileElement el3 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]");
		el3.click();
		MobileElement el4 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/datePickerImageView");
		el4.click();
		MobileElement el5 = (MobileElement) driver.findElementById("android:id/button1");
		el5.click();
		MobileElement el6 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/viewGiftButton");
		el6.click();
		log.info("Gift finder is submitted");
		MobileElement el7 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/product_preview");
		el7.click();
		log.info("A product is selected from gift finder results page");
		MobileElement el8 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/description");
		el8.click();
		log.info("Product is added to cart");
		MobileElement el9 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/addAddressButton");
		el9.click();
		log.info("Adding delivery address information...");
		MobileElement el10 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/firstNameforAddr");
		el10.sendKeys("Sandeep");
		MobileElement el11 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/lastNameforAddr");
		el11.sendKeys("Nalla");
		MobileElement el13 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/address1View");
		el13.sendKeys("1st drive");
		MobileElement el14 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/cityView");
		el14.sendKeys("New York");
		MobileElement el15 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/delPhoneNoView");
		el15.sendKeys("9533359723");
/*		try {
			((AppiumDriver) driver).hideKeyboard();
		} catch (WebDriverException exception) {
			exception.printStackTrace();
		}*/
		MobileElement el16 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/addAddressContinueView");
		el16 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/addAddressContinueView");
		el16.click();
		log.info("Submitted delivery address");
		MobileElement el17 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/useAddrRadioButton");
		el17.click();
		MobileElement el18 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/continueAdrsSuggestionView");
		el18.click();
		log.info("Continued with entered address suggestion");
		MobileElement el19 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/giftmess");
		el19.click();
		log.info("Entering gift message details...");
		MobileElement el20 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/occasionTypeEditText");
		el20.click();
		MobileElement el21 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView");
		el21.click();
		MobileElement el22 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[3]");
		el22.click();
		MobileElement el23 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/addGiftMessageTextView");
		el23.click();
		log.info("Gift message section is submitted");
		scrollDown();
		log.info("Entering Credit card and billing details");
		MobileElement el24 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/creditCardRadioButton");
		el24.click();
		MobileElement el25 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/cardNumberView");
		el25.sendKeys("4111111111111111");
		MobileElement el26 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/expireDateView");
		el26.sendKeys("11/21");
		MobileElement el27 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/cvvView");
		el27.sendKeys("123");
		MobileElement el28 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/cardHolderNameView");
		el28.sendKeys("Sandeep");
		MobileElement el29 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/firstNameforAddr");
		el29.sendKeys("Sandeep");
		MobileElement el30 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/lastNameforAddr");
		el30.sendKeys("Nalla");
		MobileElement el31 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/address1View");
		el31.sendKeys("1st drive");
		scrollDown();
		MobileElement el32 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/cityView");
		el32.sendKeys("Downers Grove");
		MobileElement el33 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/stateEditText");
		el33.click();
		MobileElement el34 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
		el34.click();
		MobileElement el35 = (MobileElement) driver.findElementById("com.sincerely.android.proflowers:id/zipcodeView");
		el35.sendKeys("60515");
		MobileElement el36 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/delPhoneNoView");
		el36.sendKeys("7416584076");
		MobileElement el37 = (MobileElement) driver
				.findElementById("com.sincerely.android.proflowers:id/confirmCardDetailsBtn");
		el37.click();
		log.info("Submitted billing section");
	}

	private void scrollDown() {
		// if pressX was zero it didn't work for me
		int pressX = driver.manage().window().getSize().width / 2;
		// 4/5 of the screen as the bottom finger-press point
		int bottomY = driver.manage().window().getSize().height * 4 / 5;
		// just non zero point, as it didn't scroll to zero normally
		int topY = driver.manage().window().getSize().height / 8;
		// scroll with TouchAction by itself
		scroll(pressX, bottomY, pressX, topY);
	}

	/*
	 * don't forget that it's "natural scroll" where fromY is the point where you
	 * press the and toY where you release it
	 */
	private void scroll(int fromX, int fromY, int toX, int toY) {
		AndroidTouchAction touchAction = new AndroidTouchAction(driver);
		touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();
	}
}